package merchan.facci.myapplication.Model;

public class User {

    private String id;
    private String usuario;
    private String imageURL;
    private String search;

    public User(String id, String usuario, String imageURL, String search) {
        this.id = id;
        this.usuario = usuario;
        this.imageURL = imageURL;
        this.search = search;
    }

    public User() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }
}
