package merchan.facci.myapplication;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import java.util.HashMap;

import merchan.facci.myapplication.Model.User;

public class RegisterActivity extends AppCompatActivity {

    EditText username, email, password;
    Button btn_register;


    FirebaseAuth auth;
    DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Registro");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        username = (EditText) findViewById(R.id.username);
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        btn_register = (Button) findViewById(R.id.btn_register);

        auth = FirebaseAuth.getInstance();

        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String txt_usuario = username.getText().toString();
                String txt_correo = email.getText().toString();
                String txt_contraseña = password.getText().toString();

                if (TextUtils.isEmpty(txt_usuario) || TextUtils.isEmpty(txt_correo) || TextUtils.isEmpty(txt_contraseña)) {
                    Toast.makeText(RegisterActivity.this, "ESTE CAMPO NO PUEDE ESTAR VACIO", Toast.LENGTH_SHORT).show();
                }else if (txt_contraseña.length()<6){
                    Toast.makeText(RegisterActivity.this,"ERROR: LA CONTRASEÑA DEBE TENER MINIMO 6 CARACTERES",Toast.LENGTH_SHORT).show();
                }else {
                    register(txt_usuario,txt_correo, txt_contraseña);
                }

            }
        });


    }

    private void register(final String username, String email, String password){

        auth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            FirebaseUser firebaseUser = auth.getCurrentUser();
                            assert firebaseUser != null;
                            String userid = firebaseUser.getUid();

                            reference = FirebaseDatabase.getInstance().getReference("Usuarios").child(userid);


                            User user = new User();
                            user.setId(userid);
                            user.setImageURL("default");
                            user.setUsuario(username);
                            user.setSearch(username.toLowerCase());

                            reference.setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()){
                                        Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                        finish();
                                    }
                                }
                            });
                        }else {
                            Toast.makeText(RegisterActivity.this,"tienes problemas con el correo o contrasena",Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}
